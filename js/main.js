var deepLink = {},
	myReq = {},
	myData = {},
	localStorage,
	datalayer = [],
	myUrl = "https://money-api-pre.yaap.com/api/paymentLink/check";

var isNumberKey = function(evt) {
	var charCode = (evt.which) ? evt.which : event.keyCode;
	if (charCode > 31 && (charCode < 48 || charCode > 57)) {
		return false;
	} else {
		return true;
	}
};

var getInputVal = function(inputID) {
	var inputElement = $(inputID);
	if (!inputElement.val()) {
		inputElement.parent().addClass('has-error');
		return '';
	} else {
		inputElement.parent().removeClass('has-error');
	}
	return inputElement.val();
};

$(function() {
	$(document).on("change", ".change-lang", function() {
		if ($(".change-lang option:selected").val() == "Es") {
			$(".lang-ca").css('display', 'none');
			$(".lang-es").css('display', 'block');
		} else if ($(".change-lang option:selected").val() == "Ca") {
			$(".lang-ca").css('display', 'block');
			$(".lang-es").css('display', 'none');
		}
	});

	$(document).on("click", "#SMSNumber", function() {
		datalayer.push("PayDeeplink", "Click_Phone", "PayDeeplink-Phone");
	});
	$(document).on("keyup", "#SMSNumber", function() {
		var firstChar = $(this).val().substr(0, 1);
		if ($(this).val().length == 9 && (firstChar == "7" || firstChar == "6")) {
			$("#sendSMS").removeClass('inverseGreenBtnDisabled');
			$("#sendSMS").removeAttr('disabled');
			$("#sendSMS").addClass('greenBtnMd');
		} else if ($(this).val().length == 0) {
			$("#sendSMS").addClass('inverseGreenBtnDisabled');
			$("#sendSMS").removeClass('greenBtnMd');
			$("#sendSMS").attr('disabled', 'disabled');
		} else {
			$("#sendSMS").addClass('inverseGreenBtnDisabled');
			$("#sendSMS").removeClass('greenBtnMd');
			$("#sendSMS").removeAttr('disabled');
		}
	});

	$('#sendSMS').click(function() {
		if ($(this).hasClass('inverseGreenBtnDisabled') === false) {
			var smsOptions = {};
			var phone = "+34 " + getInputVal('#SMSNumber');
			$("#sendSMS").text("Enviando");
			branch.sendSMS(phone, myReq, function(err, data) {
				if (err != null) {
					$(".error-box-text").show();
					$("#sendSMS").text("Enviar");
				} else {
					$("#sendSMS").text("Volver a enviar");
				}
				console.log(err || JSON.stringify(data) || 'undefined');
			});
		} else {
			$(".error-box-number").show();
		}
	});
});

(function(b, r, a, n, c, h, _, s, d, k) {
	if (!b[n] || !b[n]._q) {
		for (; s < _.length;) c(h, _[s++]);
		d = r.createElement(a);
		d.async = 1;
		d.src = "https://cdn.branch.io/branch-latest.min.js";
		k = r.getElementsByTagName(a)[0];
		k.parentNode.insertBefore(d, k);
		b[n] = h
	}
})(window, document, "script", "branch", function(b, r) {
	b[r] = function() {
		b._q.push([r, arguments])
	}
}, {
	_q: [],
	_v: 1
}, "addListener applyCode banner closeBanner creditHistory credits data deepview deepviewCta first getCode init link logout redeem referrals removeListener sendSMS setIdentity track validateCode".split(" "), 0);

// Note that this example is using the key in two places, here and below
//key_live_fgn8gxuR655bbgTbeCigLcljzCf5LW0x
branch.init('key_test_hpg3cyqS78ZmjiQfbEaaRjodwso5J6Ze', function(res, req) {
	myReq = req;
	myData = req.data;
	deepLink = req.data_parsed;
	if (req.referring_link === null) {
		deepLink = JSON.parse(localStorage.getItem("deepLink"));
	} else {
		localStorage.setItem("deepLink", JSON.stringify(deepLink));
	}
	checkLink();
});

function checkLink() {
	var dataToSend = {};
	try {
		if (deepLink.customParameters.amount !== null) {
			dataToSend = {
				'id': deepLink.customParameters.id,
				'url': myReq.referring_link,
				'text': deepLink.customParameters.text,
				'amount': deepLink.customParameters.amount,
				'userId': deepLink.customParameters.userId
			}
		} else {
			dataToSend = {
				'id': deepLink.customParameters.id,
				'url': myReq.referring_link,
				'text': deepLink.customParameters.text,
				'amount': "",
				'userId': deepLink.customParameters.userId
			}
		}
	} catch (err) {
		$(".error-box-text").show();
	}
	callCheck(dataToSend);
}

function callCheck(data) {
	var xhr = new XMLHttpRequest();
	xhr.open("POST", myUrl, true);
	xhr.setRequestHeader('Content-Type', 'application/json; charset=UTF-8');
	xhr.onreadystatechange = function() {
		if (xhr.readyState == 4 && xhr.status == 200) {
			var response = JSON.parse(xhr.responseText);
			if (response.infoResponse.code == "OK") {
				datalayer.push("PayDeeplink", "Send_OK", "PayDeeplink-Phone");
				printData(response.responseObject);
			} else {
				$(".error-box-text").show();
				datalayer.push("PayDeeplink", "Send_KO_[" + response.infoResponse.code + "," + response.infoResponse.text + "]", "PayDeeplink-Phone");
			}
		} else if (xhr.status !== 200) {
			$(".error-box-text").show();
		}
	};
	// send the collected data as JSON
	xhr.send(JSON.stringify(data));
}

function printData(information) {
	$("#user").text(information.userInformation.name);
	if (information.paymentLink.amount !== null) {
		$("#amount").text(information.paymentLink.amount + " €");
	} else {
		$("#amount").hide();
	}

	//key_live_fgn8gxuR655bbgTbeCigLcljzCf5LW0x
	var info = 'key_test_hpg3cyqS78ZmjiQfbEaaRjodwso5J6Ze';
	var sampleParams = {
		data: deepLink
	};
}